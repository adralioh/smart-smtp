#!/usr/bin/env sh
# Wrapper script for smart_smtp.py that allows you to use a custom path for the config file
# This script should be given to `smartd.conf`, eg:
#   DEVICESCAN -a -m alerts@example.org -M exec /path/to/this/script
export SMART_SMTP_CONFIG=/path/to/smart_smtp.conf
/path/to/smart_smtp.py "$@"
