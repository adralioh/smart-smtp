# smart-smtp
Script that allows smartd to send email alerts via SMTP

Normally, smartd sends email via your system's `mail` command, which requires a local mail server to be set up. \
This script allows you to send them via SMTP instead.

## Requirements
- Python 3.6+

## Configuration

### `smartd.conf`
The path to this script should be given as the `-M exec PATH` option in `smartd.conf`. You also need to specify email addresses via the `-m ADD` option as normal.
```
# /etc/smartd.conf
DEVICESCAN -a -m alerts@example.org -M exec /path/to/smart_smtp.py
```

### Config file
This script requires a config file to function.

See `smart_smtp.conf` for an example config file.

By default, the script will look for the file `smart_smtp.conf` in the same directory as the script.

If you want to use a different path, you can manually specify the path via the `SMART_SMTP_CONFIG` env var. \
This can be done using a wrapper script that is called by smartd. See `wrapper.sh` for an example.

## Testing
You can verify that the script works by passing the `-M test` option to `smartd.conf`. This will send a test email.
```
# /etc/smartd.conf
DEVICESCAN -m alerts@example.org -M exec /path/to/smart_smtp.py -M test
```
