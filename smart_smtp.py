#!/usr/bin/env python3
"""Send alerts generated by smartd via SMTP

The path to this script should be given as the `-M exec PATH` option in
smartd.conf, eg, `-m alerts@example.org -M exec /path/to/smart_smtp.py`

Config file path:
    This script requires a config file to function.

    Since smartd doesn't allow passing args, the following options are
    used to determine the config file path:
        Command-line arg:
            Pass via the '-c' argument. eg,
            `smart_smtp.py -c /path/to/config`

            This can be passed via a wrapper script called by smartd
        Env var:
            Set the env var 'SMART_SMTP_CONFIG' to the config file path

            You may need to use a wrapper script for this as well, since
            smartd didn't preserve env vars in my testing
        Default path:
            If neither of the above options are given, the default path
            is used as a fallback

            The script looks for the file 'smart_smtp.conf' in the same
            dir as the script itself

            You can also manually change the `default_config_file` var
            below

Example config file:
    [smart_smtp]
    # email address to send the email from
    from = server@example.org
    # if only a domain is given, the hostname will be used as the local-part
    #from = example.org
    # hostname of the smtp server
    smtp_host = smtp.example.org
    # port that the smtp server is listening on
    smtp_port = 25
    # the type of encryption to use when connecting to the smtp server
    # options: none, starttls, ssl
    encryption = none
    # username and password to use when connecting to the smtp server. optional
    #username = user
    #password = password
"""
import argparse
import configparser
import email.message
import os
import smtplib
import socket
import ssl
import sys
import warnings

default_config_file = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'smart_smtp.conf'
)
"""Path to the config file

Used if the path isn't given via env var or as a command-line arg

Defaults to '/same/dir/as/script/smart_smtp.conf'
"""


def _get_parser():
    """Returns the ArgumentParser used by main()"""
    parser = argparse.ArgumentParser(
        description='send smartd alerts via smtp',
        epilog="see the doc string in '%(prog)s' for more information"
    )

    config_group = parser.add_argument_group(
        'config arguments', 'arguments relating to the config file'
    )
    config_group.add_argument('-c', '--config', help='path to config file')
    config_group.add_argument(
        '--print-config', action='store_true',
        help='print an example config file and exit'
    )

    smart_group = parser.add_argument_group(
        'smartd arguments',
        'arguments given by smartd. should not be given manually'
    )
    smart_group.add_argument(
        '-s', dest='subject', default='', help='email subject'
    )
    smart_group.add_argument('address', nargs='*', help='email to-address')

    return parser


def get_smart_env():
    """Return a dict that contains all the env vars set by smartd"""
    smart_map = {
        'SMARTD_MAILER': 'mailer',
        'SMARTD_DEVICE': 'device',
        'SMARTD_DEVICETYPE': 'device_type',
        'SMARTD_DEVICESTRING': 'device_string',
        'SMARTD_DEVICEINFO': 'device_info',
        'SMARTD_FAILTYPE': 'fail_type',
        'SMARTD_ADDRESS': 'address',
        'SMARTD_MESSAGE': 'message',
        'SMARTD_FULLMESSAGE': 'full_message',
        'SMARTD_TFIRST': 'time_first',
        'SMARTD_TFIRSTEPOCH': 'time_first_epoch',
        'SMARTD_PREVCNT': 'previous_count',
        'SMARTD_NEXTDAYS': 'next_days',
        'SMARTD_SUBJECT': 'subject'
    }
    smart_env = {}
    missing_env = False

    for env, name in smart_map.items():
        try:
            smart_env[name] = os.environ[env]
        except KeyError:
            # Most of the env vars aren't used, so we'll try to continue
            # anyway if any are missing
            warnings.warn(f'Missing SMARTD env var: {env}')
            missing_env = True

    # If env vars are missing, it's probably because the script was run
    # directly, so we'll advise the user to run it through smartd
    if missing_env:
        print('Are you running the script through smartd?', file=sys.stderr)

    return smart_env


def send_message(
    *, to, from_, subject='', body='',
    smtp_host='', smtp_port=0, encryption='none', username=None, password=None
):
    """Send a message via SMTP using the given args

    to (str or sequence of str):
        the email address(es) to send the email to

        can be a single address, or a list of addresses
    from_ (str):
        the email address to send the email from

        if only the domain is given, the computer's hostname will be
        used as the local-part
    subject (str)
        subject of the email
    body (str):
        body of the email
    smtp_host (str):
        smtp server hostname
    smtp_port (int):
        smtp server port
    encryption ('none', 'starttls', or 'ssl'):
        what type of encryption to use when connecting to the smtp
        server

        none: no encryption. email will be sent over plaintext
        starttls: starttls will be used after connecting
        ssl: ssl will be used from the start
    username (str):
        username to use to log into the smtp server
    password (str):
        password to use to log into the smtp server
    """
    if encryption not in {'none', 'starttls', 'ssl'}:
        raise ValueError(f'unsupported encryption method: {encryption}')

    msg = email.message.EmailMessage()

    if isinstance(to, str):
        msg['To'] = to
    else:
        msg['To'] = ', '.join(to)

    if '@' in from_:
        msg['From'] = from_
    else:
        hostname = socket.gethostname().split('.', 1)[0]
        msg['From'] = f'{hostname}@{from_}'

    msg['Subject'] = subject
    msg.set_content(body)

    if encryption in {'ssl', 'starttls'}:
        # Recommended by
        # https://docs.python.org/3/library/ssl.html#ssl-security
        context = ssl.create_default_context()

    if encryption == 'ssl':
        smtp = smtplib.SMTP_SSL(smtp_host, smtp_port, context=context)
    else:
        smtp = smtplib.SMTP(smtp_host, smtp_port)

    if encryption == 'starttls':
        smtp.starttls(context=context)

    if username is not None:
        smtp.login(username, password)

    smtp.send_message(msg)
    smtp.quit()


def write_config(file=sys.stdout):
    """Writes an example config file to the given file object

    Defaults to stdout
    """
    file.write(
        '[smart_smtp]\n'
        '# email address to send the email from\n'
        'from = server@example.org\n'
        '# if only a domain is given, the hostname will be used as the '
        'local-part\n'
        '#from = example.org\n'
        '# hostname of the smtp server\n'
        'smtp_host = smtp.example.org\n'
        '# port that the smtp server is listening on\n'
        'smtp_port = 25\n'
        '# the type of encryption to use when connecting to the smtp server\n'
        '# options: none, starttls, ssl\n'
        'encryption = none\n'
        '# username and password to use when connecting to the smtp server. '
        'optional\n'
        '#username = user\n'
        '#password = password\n'
    )


def parse_config(config_file):
    """Parse and return the config file at the given path"""
    config = configparser.ConfigParser()
    with open(config_file, 'r') as file:
        config.read_file(file)
    return config


def main():
    parser = _get_parser()
    args = parser.parse_args()

    # Print example config file and exit
    if args.print_config:
        write_config()
        sys.exit()

    # Required when --print-config isn't given
    if not args.address:
        parser.error('the following argument is required: address')

    # Attempt to find and parse the config file
    if args.config is not None:  # command-line arg
        config_path = args.config
    elif 'SMART_SMTP_CONFIG' in os.environ:  # env var
        config_path = os.environ['SMART_SMTP_CONFIG']
    else:  # default fallback path
        config_path = default_config_file
    config = parse_config(config_path)

    smart_env = get_smart_env()

    # Write debug info to temp file
    # test_file = open('/tmp/smart_test', 'w')
    # test_file.write(f'ARGS: {args}\n')
    # test_file.write(f'CONFIG_PATH: {config_path}\n')
    # test_file.write(f'CONFIG: {dict(config.items("smart_smtp"))}\n')
    # for env, value in smart_env.items():
    #     test_file.write(f'{env}: {repr(value)}\n')
    # test_file.close()

    send_message(
        to=args.address,
        from_=config.get('smart_smtp', 'from'),
        subject=args.subject,
        body=smart_env['full_message'],
        smtp_host=config.get('smart_smtp', 'smtp_host'),
        smtp_port=config.getint('smart_smtp', 'smtp_port'),
        encryption=config.get('smart_smtp', 'encryption', fallback='none'),
        username=config.get('smart_smtp', 'username', fallback=None),
        password=config.get('smart_smtp', 'password', fallback=None)
    )


if __name__ == '__main__':
    main()
