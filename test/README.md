Docker container for testing the script. Intended for contributors.

When you run the container, smartd will send a test email for each drive it detects, then exit

If you need a SMTP server to send test emails to, you can use a dummy SMTP server such as
[fake-smtp-server](https://github.com/gessnerfl/fake-smtp-server)

## Usage
1.  Copy the script and config file to the volume dir, and make any desired changes to them
    ```bash
    cp ../smart_smtp.{py,conf} volume/
    ```

    The volume dir also contains the smartd conf, where you can specify the email address to send to

1.  Build the container

    The Python version is passed via the `VERSION` arg. In this case, 3.6 is used
    ```bash
    docker build -t smart-smtp --build-arg=VERSION=3.6 .
    ```

1.  Run the container
    ```bash
    docker run --rm --privileged -v "$PWD/volume":/usr/src/myapp:ro smart-smtp
    ```

    smartd will fail to run if it's unable to find any SMART-enabled devices. \
    If this happens, make sure that the container is running in privileged mode, and that
    your computer has a drive that supports SMART
